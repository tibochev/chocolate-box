#!/usr/local/bin/python
import sys
import os
import matplotlib
matplotlib.use("TkAgg")

current_dir = os.getcwd()
lib_path = os.path.join(current_dir, "lib")
sys.path.append(lib_path)

import box

if __name__ == '__main__':
  nb_column = 16
  nb_row = 4

#  nb_chocolate = 10

  chocolate_box = box.box(nb_row, nb_column, main_path=current_dir);

#  print(chocolate_box)

#  chocolate_box.fill_box_random(nb_chocolate, verbose=True)

#  print(chocolate_box)
#  
#  row,col = chocolate_box.get_random_chocolate(verbose=True)
#  chocolate_box.eat_chocolate(row,col,verbose=True)

#  print(chocolate_box)
#  chocolate_box.get_figure()
