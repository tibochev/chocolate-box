import tkinter as tk
from tkinter import simpledialog
from tkinter import ttk
from PIL import Image
from PIL import ImageTk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import os
import random as rd
from CreateCanvasObject import CreateCanvasObject

class gui:
  """ Tool to make a gui of the chocolate box """
  
  def __init__(self, chocolate_box, path):
    self.__main_path = path
    
    self.__chocolate_box = chocolate_box
    # dimensions
    self.__canvas_width = 1000
    self.__canvas_margin_left = 40
    self.__canvas_margin_right = 10
    self.__canvas_margin_top = 10
    self.__canvas_margin_bottom = 40
    # Height is redefined based on ratio of row and columns
    self.__canvas_height = self.__canvas_width*(
        self.__chocolate_box.get_nb_row()/self.__chocolate_box.get_nb_column()
      )
    # Size of a chocolate square box is defined from total size and number
    # of rows and columns
    self.__square_width = min(
          (self.__canvas_width 
            - (self.__canvas_margin_left
             + self.__canvas_margin_right)
          )/self.__chocolate_box.get_nb_column()
        ,
          (self.__canvas_height 
            - (self.__canvas_margin_top
             + self.__canvas_margin_bottom)
          )/self.__chocolate_box.get_nb_row()
       )
    
    #Containers
    self.__images_by_positions = {}  # Keeps track of where we drew chocolates
                                     # (stores the image objects directly)
    for row in range(self.__chocolate_box.get_nb_row()):
      for column in range(self.__chocolate_box.get_nb_column()):
        self.__images_by_positions[row,column] = None
    # All the buttons and check buttons that will be defined are stored here
    self.__buttons = {}
    self.__check_buttons = {}
    self.__possible_chocolate_types = [] # List of n 0 or 1, n being the number of
                                    # different kind of chocolates, 0 or 1
                                    # telling whether they are selected for
                                    # drawing or not
    
    #Other attributes
    
    # Main gui objects
    self.__thegui = tk.Tk()
    self.__thegui.title("Chocolate box")
    self.__window = tk.Frame(
      self.__thegui,
      borderwidth=2,
      relief="sunken",
      bd=10,
    )
    self.__thegui.protocol("WM_DELETE_WINDOW", self.close)
    self.__window.grid()
    
    #Icons
    self.__chocolate_images_path = [
        os.path.join(
          self.__main_path,
          "pic",
          str(i+1).join(("chocolate_",".gif"))
        )
        for i in range(self.__chocolate_box.get_nb_chocolate_types())
      ]
    self.__chocolate_images = [
        ImageTk.PhotoImage(
          Image.open(path).resize(
                                (int(self.__square_width*0.9),
                                 int(self.__square_width*0.9)),
                                Image.ANTIALIAS
                            )
        )
      for path in self.__chocolate_images_path
    ]
    if len(self.__chocolate_images) == 0:
      raise ValueError("Could not find chocolate icons in pic/")
    self.__thegui.tk.call('wm',
                          'iconphoto',
                           self.__thegui._w,
                           self.__chocolate_images[0])
    self.__possible_chocolate_types = [tk.IntVar(value = int(i == 0)) for i in
                                  range(len(self.__chocolate_images))]
    # Square box icon
    self.__square_box_path = os.path.join(
      self.__main_path,
      "pic/single_box_square.gif"
    )
    self.__square_box_image = ImageTk.PhotoImage(
        Image.open(self.__square_box_path).resize(
          (int(self.__square_width),
           int(self.__square_width)),
          Image.ANTIALIAS
        )
      )
    
    # Action buttons
    self._make_button(
        "Fill box random",
         self.fill_box_random_prompt
      )
    self._make_button(
        "Add random chocolate",
        self.add_random_chocolate
      )
    self._make_button(
        "Move to adjacent (random)",
        self.move_to_adjacent_random
      )
    self._make_button(
        "Move random chocolate",
        self.move_random_chocolate
      )
    self._make_button(
        "Eat random chocolate",
        self.eat_random_chocolate
      )
    self._make_button(
        "Eat all selected types",
        self.eat_all_selected_types
      )
    self._make_button(
        "print",
        lambda : print(self.__chocolate_box)
      )
    self._make_button(
        "Print nb chocolates",
        self.print_nb_chocolates
      )
    self._make_button(
        "Reset (shuffle) box",
        self.reset
      )
    self._make_button(
        "Empty box",
        self.empty
      )
    self._make_button(
        "Close",
        self.close,
        textcolor="red"
      )
    # Checker buttons to select chocolate
    for i in range(len(self.__chocolate_images)):
      self._make_check_button(i)
    
    # Canvas to show the box
    self.__canvas = tk.Canvas(
      self.__window,
      height=self.__canvas_height,
      width=self.__canvas_width
    )
    self.checkered()
    self.__canvas.grid(row=0,
                       column=1,
                       rowspan=len(self.__buttons)-1,
                       columnspan=max(len(self.__check_buttons)-1,1))
    self.__canvas.bind("<Button-3>", self.add_or_eat_chocolate_click)

  def print_nb_chocolates(self):
    print("total:", self.__chocolate_box.get_nb_chocolates())
    print(self.__chocolate_box.get_nb_chocolates_per_type())
    for i in range(len(self.__chocolate_box.get_nb_chocolates_per_type())):
      print("type", i, ":", self.__chocolate_box.get_nb_chocolates_per_type()[i])

  def get_possible_chocolate_types(self):
    
    return [i for i in range(len(self.__possible_chocolate_types))
               if self.__possible_chocolate_types[i].get() == 1]

  def _make_button(self, text, command, textcolor=None):
    """ Creates a button is self.__window """
    self.__buttons[text] = tk.Button(
      self.__window,
      text=text,
      command = command,
      fg=textcolor,
      width=15,
      wraplength=100
    )
    self.__buttons[text].grid(column=0)

  def _make_check_button(self, i):
    """ Creates check button for chocolate selection.
    
    Binds it to self.__possible_chocolate_types 
    """
    self.__check_buttons[i] = tk.Checkbutton(
      self.__window,
      image=self.__chocolate_images[i],
      variable=self.__possible_chocolate_types[i],
      command=self.update_possible_types
    )
    self.__check_buttons[i].grid(
      row = len(self.__buttons),
      column=i
    )
    if i == len(self.__chocolate_images) - 1:
      self.__check_buttons["Select all"] = tk.Button(
        self.__window,
        text="Select all",
        command=self.select_all_types,
        width=15,
        wraplength=100
      )
      self.__check_buttons["Select all"].grid(
        row = len(self.__buttons),
        column = len(self.__check_buttons)-1
      )
      self.__check_buttons["Unselect all"] = tk.Button(
        self.__window,
        text="Unselect all",
        command=self.unselect_all_types,
        width=15,
        wraplength=100
      )
      self.__check_buttons["Unselect all"].grid(
        row = len(self.__buttons),
        column = len(self.__check_buttons)-1
      )

  def select_all_types(self):
    for item in self.__possible_chocolate_types:
      item.set(1)

  def unselect_all_types(self):
    for item in self.__possible_chocolate_types:
      item.set(0)
    self.__possible_chocolate_types[0].set(1)

  def update_possible_types(self):
    """ Called when a chocolate type checkbutton is clicked
    
    Makes sure that at least the first type is selected if all others are not.
    """
    if len([1 for i in self.__possible_chocolate_types if i.get() == 1 ]) == 0:
      self.__possible_chocolate_types[0].set(1)

  def draw_chocolate(self, choc):
    """ Draws a chocolate at given row and column.
    
    If ichoc is specified, use the corresponding icon. Else, choose
    an icon randomly in the list of selected icons.
    """
    # Converts row and column numbers to pixel coordinates. Matches the center
    # of the corresponding bin
    row = choc.get_row()
    column = choc.get_column()
    row_coordinates = (self.__canvas_margin_top
                     + row*self.__square_width
                     + self.__square_width/2)
    column_coordinates = (self.__canvas_margin_left 
                        + column*self.__square_width
                        + self.__square_width/2)
    # Create an object that can be moved around
    CreateCanvasObject(
      self.__canvas,
      self.__chocolate_images[choc.get_typeID()],
      column_coordinates,
      row_coordinates,
      self.__images_by_positions,
      self.__chocolate_box.get_nb_column(),
      self.__chocolate_box.get_nb_row(),
      self.__square_width,
      self.__canvas_margin_left,
      self.__canvas_margin_top,
      self.__chocolate_box,
      verbose=True
    )

  def erase_chocolate(self, choc):
    row = choc.get_row()
    column = choc.get_column()
    """ Removes a chocolate icon. """
    self.__images_by_positions[row,column].delete()

  def erase_all_chocolates(self):
    """ Removes all chocolate icons. """
    for item in self.__images_by_positions:
      if self.__images_by_positions[item] is not None:
        self.__images_by_positions[item].delete()

  def checkered(self):
    """ Creates the box background"""
    for x in range(self.__chocolate_box.get_nb_column()+1):
      if x < self.__chocolate_box.get_nb_column():
        self.__canvas.create_text(
          self.__canvas_margin_left + x*self.__square_width + self.__square_width/2,
          self.__canvas_margin_top
            + self.__square_width
            * self.__chocolate_box.get_nb_row()
            + self.__canvas_margin_bottom/2,
          text=str(x)
        )
    for y in range(self.__chocolate_box.get_nb_row()+1):
      if y < self.__chocolate_box.get_nb_row():
        self.__canvas.create_text(
          self.__canvas_margin_left/2,
          self.__canvas_margin_top
             + y*self.__square_width
             + self.__square_width/2,
          text=str(y)
        )
    for x in range(self.__chocolate_box.get_nb_column()):
      for y in range(self.__chocolate_box.get_nb_row()):
        column = (self.__canvas_margin_left
                + x*self.__square_width
                + self.__square_width/2)
        row =  (self.__canvas_margin_top
                + y*self.__square_width
                + self.__square_width/2)
        self.__canvas.create_image(
          column,
          row,
          image=self.__square_box_image
        )

  def fill_box_random_prompt(self):
    """ Fills the box randomly with a number of chocolate to specify
    
    Randomly chooses among the possible chocolates. Will be updated to give the
    possibility to specify the number of chocolate of each kind to generate.
    """
    nb_chocolate = simpledialog.askinteger(
       "nb chocolate",
       "How many chocolates?",
        minvalue=0,
        maxvalue=self.__chocolate_box.get_size()
      )
    if nb_chocolate is None:
      return
    self.__chocolate_box.fill_box_random(
        nb_chocolate,
        verbose=True,
      )

  def add_random_chocolate(self):
    """ Add a chocolate in a random position
    
    chooses the icon randomly among the possible icons
    """
    self.__chocolate_box.add_random_chocolate(verbose=True)

  def add_or_eat_chocolate_click(self, event):
    """ Used when right-clicking, to add or remove chocolate at mouse position
    
    chooses the icon randomly among the possible icons
    """
    xpos, ypos = event.x, event.y
    column = self.compute_column(xpos)
    row = self.compute_row(ypos)
    if not self.__chocolate_box._check_row_column(
          row,
          column,
          raise_if_false=False,
          verbose=False
        ):
      return
    choc = self.__chocolate_box[row,column]
    if choc is None:
      return False
    if choc.is_on():
      self.__chocolate_box.eat_chocolate(
          choc,
          verbose=True,
          raise_if_false=False
        )
    else:
      self.__chocolate_box.add_chocolate(
          choc,
          verbose=True,
          raise_if_false=False
        )

  def move_to_adjacent_random(self):
    """ Randomly chooses a chocolate and moves it to a random adjecent square """
    self.__chocolate_box.move_to_adjacent_random(verbose=True)

  def move_random_chocolate(self):
    """ Randomly chooses a chocolate and moves it to a random square """
    self.__chocolate_box.move_random_chocolate(verbose=True)

  def eat_random_chocolate(self):
    """ Removes a randomly chosen chocolate """
    self.__chocolate_box.eat_random_chocolate(verbose=True)

  def empty(self):
    """ Removes all chocolates """
    self.__chocolate_box.empty(verbose=True)

  def eat_all_selected_types(self):
    """ Removes all chocolates of selected types """
    self.__chocolate_box.eat_all_selected_types(verbose=True)

  def reset(self):
    """ Empties then refills the box with the same number of chocolates """
    self.__chocolate_box.reset(verbose=True)

  def mainloop(self):
    self.__thegui.mainloop()

  def close(self):
    print("Gui closed")
    self.__thegui.destroy()

  def compute_column(self,x):
    """ Converts a pixel coordinate to a column number"""
    return int((x-self.__canvas_margin_left)/self.__square_width)

  def compute_row(self,y):
    """ Converts a pixel coordinate to a row number"""
    return int((y-self.__canvas_margin_top)/self.__square_width)
