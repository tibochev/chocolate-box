class CreateCanvasObject(object):
  def __init__(self,
               canvas,
               image,
               column_coordinate,
               row_coordinate,
               images_by_positions,
               nb_column,
               nb_row,
               square_width,
               margin_left,
               margin_top,
               chocolate_box,
               verbose=False):
    self.chocolate_box = chocolate_box
    self.images_by_positions = images_by_positions
    self.nb_column = nb_column
    self.nb_row = nb_row
    self.square_width = square_width
    self.margin_left = margin_left
    self.margin_top = margin_top
    self.canvas = canvas
    self.column_coordinate = column_coordinate
    self.row_coordinate = row_coordinate
    self.column = self.compute_column()
    self.row = self.compute_row()
    self.image_obj = canvas.create_image(
        self.column_coordinate,
        self.row_coordinate,
        image=image
      )
    self.images_by_positions[self.row,self.column] = self
    canvas.tag_bind(self.image_obj, '<Button1-Motion>', self.move)
    canvas.tag_bind(self.image_obj, '<ButtonRelease-1>', self.release)
    
    self.move_flag = False
    self.verbose = verbose
       
  def move(self, event):
    if self.move_flag:
      new_xpos, new_ypos = event.x, event.y
       
      self.canvas.move(self.image_obj,
          new_xpos-self.mouse_xpos ,new_ypos-self.mouse_ypos)
       
      self.mouse_xpos = new_xpos
      self.mouse_ypos = new_ypos
    else:
      self.move_flag = True
      self.canvas.tag_raise(self.image_obj)
      self.mouse_xpos = event.x
      self.mouse_ypos = event.y
      self.canvas.move(self.image_obj,
          self.mouse_xpos-self.column_coordinate,
          self.mouse_ypos-self.row_coordinate)

  def release(self, event):
    self.move_flag = False
    new_xpos, new_ypos = event.x, event.y
    if (new_xpos <= self.margin_left
        or new_ypos <= self.margin_top
        or new_xpos >= (
            self.nb_column*self.square_width
            + self.margin_left
          )
        or new_ypos >= (
            self.nb_row*self.square_width
            + self.margin_top
          )
        ):
      
      new_xpos = self.column_coordinate
      new_ypos = self.row_coordinate
    new_column = int((new_xpos-self.margin_left)/self.square_width)
    new_row = int((new_ypos-self.margin_top)/self.square_width)
    new_xpos = (self.margin_left
              + new_column*self.square_width
              + self.square_width/2)
    new_ypos = (self.margin_top 
              + new_row*self.square_width
              + self.square_width/2)
    self.canvas.move(self.image_obj,
        new_xpos-self.mouse_xpos ,new_ypos-self.mouse_ypos)
    
    tmp_row = self.compute_row(new_ypos)
    tmp_column = self.compute_column(new_xpos)
    if not self.chocolate_box.move_chocolate(
      self.row,
      self.column,
      tmp_row,
      tmp_column,
      self.verbose,
      False
    ):
      return
    self.images_by_positions[self.row,self.column] = None
    self.column_coordinate = new_xpos
    self.row_coordinate = new_ypos
    self.column = tmp_column
    self.row = tmp_row
    self.images_by_positions[self.row, self.column] = self

  def compute_column(self,x = None):
    if x is None:
      x = self.column_coordinate
    return int((x-self.margin_left)/self.square_width)

  def compute_row(self,y = None):
    if y is None:
      y = self.row_coordinate
    return int((y-self.margin_top)/self.square_width)

  def delete(self):
    self.canvas.delete(self.image_obj)
    if (self.row,self.column) in self.images_by_positions:
      self.images_by_positions[self.row,self.column] = None
    else:
      return False
    return True
