 #!/usr/local/bin/python

import random
import warnings
import matplotlib.pyplot as plt
from copy import deepcopy
from gui import gui
import os

from chocolate import chocolate

class box:
  """
    box: 2D list of chocolates object, initiallised to OFF
    
    Keeps track of chocolates positions ON and OFF separately
    for easy random choosing
  """
  def __init__(self, nb_row, nb_column, main_path):
    self.__nb_chocolate_types = 5
    self.__nb_row = nb_row
    self.__nb_column = nb_column
    self.__size = self.__nb_row * self.__nb_column
    self.__box = []
    self.__nb_chocolates = 0
    self.__nb_chocolates_per_type = [0]*self.__nb_chocolate_types
    self.__on_chocolates = []
    self.__on_chocolates_per_type = [[] for i in range(self.__nb_chocolate_types)]
    self.__off_chocolates = []
    for row in range(self.__nb_row):
      self.__box.append([])
      for column in range(self.__nb_column):
        self.__box[-1].append(chocolate(row=row,column=column,onoff="OFF"))
        self.__off_chocolates.append(self.__box[-1][-1])
    self.__gui = None
    self.__main_path = main_path # Path to main project dir.
    self.make_gui()

  def __str__(self):
    """
      string conversion overload
    """
    separation_line = " "
    for c in range(self.__nb_column):
      separation_line = "".join((separation_line, "- "))
    
    printout = " "
    printout = "".join((printout, separation_line))
    printout = "".join((printout, "\n"))
    for r in range(self.__nb_row):
      for c in range(self.__nb_column):
        outchar = "x"
        if self[r,c].is_on():
          outchar = str(self[r,c].get_typeID())
        printout = "|".join((printout, outchar))
      printout = "".join((printout, "|\n "))
      printout = "".join((printout, separation_line))
      printout = "".join((printout, "\n"))
    return printout

  def __contains__(self, rowcol):
    """ in operation overload
    
    Must provide a tuple (row,colum). If the row and column are not in range
    of the box, returns False. Else, if the chocolate at the given 
    coordinates is ON, returns True. Else returns False.
    """
    if not self._check_row_column(
          rowcol[0],
          rowcol[1],
          verbose=False,
          raise_if_false=False
        ):
      return False
    return self[rowcol].is_on()

  def __getitem__(self, rowcol):
    """ getter [] overload.
    
    Must provide a tuple (row,column). If the row and column are not in range
    of the box, returns None.
    """
    if len(rowcol) != 2:
      raise ValueError("Need column and row indexes to access box content")
    if not self._check_row_column(
          rowcol[0],
          rowcol[1],
          verbose=False,
          raise_if_false=False
        ):
      return None
    return self.__box[rowcol[0]][rowcol[1]]

  def __setitem__(self,rowcol,value):
    """ setter [] overload.
    
    Must provide a tuple (row,column). If the row and column are not in range
    of the box, raise ValueError.
    Give \"ON\" to add a chocolate, \"OFF\" to remove it. Append v to be verbose.
     """
    worked = False
    self._check_row_column(
          rowcol[0],
          rowcol[1],
          verbose=True,
          raise_if_false=True
        )
    choc= self[rowcol]
    if choc is None:
      raise ValueError("Problem setting item at", rowcol)
    if not isinstance(value,str):
      raise ValueError("Must be string : ON, OFF, ONv or OFFv")
    if value == "OFF":
      worked = self.eat_chocolate(choc,
          verbose=False, raise_if_false=True)
    elif value == "ON":
      worked = self.add_chocolate(choc,
          verbose=False, raise_if_false=True)
    elif value == "OFFv":
      worked = self.eat_chocolate(choc,
          verbose=True, raise_if_false=True)
    elif value == "ONv":
      worked = self.add_chocolate(choc,
          verbose=True, raise_if_false=True)
    else:
      raise ValueError("Must be string : ON, OFF, ONv or OFFv")

  def reset(self, verbose=False, raise_if_false=True):
    """ Empties and refills randomly the box """
    nb_chocolate = self.__nb_chocolates
    tmp_nb_chocolates_per_type = deepcopy(self.__nb_chocolates_per_type)
    self.empty()
    self.fill_box_random(nb_chocolate, 
                         verbose=False,
                         raise_if_false=raise_if_false,
                         nb_chocolates_per_type=tmp_nb_chocolates_per_type)
    if verbose:
      print("Box reset")
  
  def empty(self, verbose=False, raise_if_false=True):
    """ Empties the box """
    for choc in self.__on_chocolates[:]:
      self.eat_chocolate(choc, verbose=False, raise_if_false=raise_if_false)
    if verbose:
      print("Box emptied.")

  def make_gui(self):
    self.__gui = gui(self, self.__main_path)
    self.__gui.mainloop()

  # Accessors
  def get_nb_row(self):
    """ Returns the number of rows """
    return self.__nb_row

  def get_nb_column(self):
    """ Returns the number of columns """
    return self.__nb_column

  def get_size(self):
    """ Returns the size (rows x columns) """
    return self.__size

  def get_box(self, copy=True):
    """
      Returns the box np.array object.
      If copy is set to false, modyfing the returned object will modify box.
      Else, uses deepcopy to return a new, independant object.
    """
    if copy:
      return deepcopy(self.__box)
    return self.__box

  def get_nb_chocolates(self):
    """ Returns the number of chocolate in the box """
    return self.__nb_chocolates

  def get_nb_chocolate_types(self):
    """ Returns the number of possible chocolate type """
    return self.__nb_chocolate_types

  def get_nb_chocolates_per_type(self, copy=True):
    """ Returns the number of possible chocolate type """
    if copy:
      return deepcopy(self.__nb_chocolates_per_type)
    return self.__nb_chocolates_per_type

  def get_random_chocolate(self, verbose=False, typeID = -1):
    """ 
      Returns a randomly chosen chocolate among all ON chocolates
      
      If box is empty, returns None
    """
    choc= None
    if self.__nb_chocolates == 0:
      warnings.warn("Empty box, cannot get a random chocolate!", Warning)
      return choc
    if typeID < 0:
      possible_ids = [ID for ID in self.__gui.get_possible_chocolate_types() 
                      if self.__nb_chocolates_per_type[ID] != 0]
      if len(possible_ids) == 0:
        warnings.warn("No chocolates left for selected types.", Warning)
        return choc
      typeID = random.choice(possible_ids)
    if len(self.__on_chocolates_per_type[typeID]) == 0:
      warnings.warn("No chocolates left for selected type.", Warning)
      return choc
    choc = random.choice(self.__on_chocolates_per_type[typeID])
    if verbose:
      print("Got", choc)
    return choc

  def get_empty_chocolate(self, verbose=False):
    """ 
      Returns a randomly chosen chocolate among all OFF chocolates
      
      If box is full, returns None
    """
    choc = None
    if self.__nb_chocolates == self.__size:
      warnings.warn("Full box, cannot get a random empty coordinate!", Warning)
      return choc
    choc = random.choice(self.__off_chocolates)
    if verbose:
      print("Got", choc)
    return choc

  def get_on_chocolates(self, copy=True):
    """
      Like get_random_chocolate but for all chocolates.
      If copy is set to false, modyfing the returned object will modify box.
      Else, uses deepcopy to return a new, independant object.
    """
    if copy:
      return deepcopy(self.__on_chocolates)
    return self.__on_chocolates

  def get_on_chocolates_per_type(self, copy=True):
    """
      Like get_on_chocolates but sperated by type.
    """
    if copy:
      return deepcopy(self.__on_chocolates_per_type)
    return self.__on_chocolates_per_type

  def get_off_chocolates(self, copy=True):
    """
      Like get_random_empty but for all empty coordinates.
      If copy is set to false, modyfing the returned object will modify box.
      Else, uses deepcopy to return a new, independant object.
    """
    if copy:
      return deepcopy(self.__off_chocolates)
    return self.__off_chocolates

  # Filling methods
  def fill_box_random(self,
                      nb_chocolate,
                      verbose=False,
                      raise_if_false=True,
                      nb_chocolates_per_type = []):
    """
      Adds nb_chocolates at random coordinates
    """
    if nb_chocolates_per_type == []:
      for i in range(nb_chocolate):
        if self.add_random_chocolate(verbose, raise_if_false) is None:
          return
    else:
      for typeID in range(len(nb_chocolates_per_type)):
        for i in range(nb_chocolates_per_type[typeID]):
          if self.add_random_chocolate(verbose, raise_if_false, typeID) is None:
            return

  def add_random_chocolate(self, verbose=False, raise_if_false=True, typeID = -1):
    """
      change a random OFF chocolate to ON chocolate if possible, and 
      returns it. If not possible, returns None
    """
    choc = None
    if self.__nb_chocolates == self.__size:
      warnings.warn("Box is full", Warning)
      return choc
    choc = random.choice(self.__off_chocolates)
    if not self.add_chocolate(choc,
                              verbose=verbose,
                              raise_if_false=raise_if_false,
                              typeID=typeID):
      choc = None
    return choc

  def add_chocolate(self, choc, verbose=False, raise_if_false=True, typeID=-1):
    """ Change chocolate to ON and uptades every containers and GUI.
    """
    if choc is None:
      return False
    if choc.is_on():
      warnings.warn("There is already a chocolate here!", Warning)
      print(choc)
      return False
    choc.on()
    if typeID < 0:
      typeID = random.choice(self.__gui.get_possible_chocolate_types())
    choc.set_typeID(typeID)
    self.__nb_chocolates += 1
    self.__nb_chocolates_per_type[typeID] += 1
    self.__on_chocolates.append(choc)
    self.__on_chocolates_per_type[typeID].append(choc)
    self.__off_chocolates.remove(choc)
    self.__gui.draw_chocolate(choc)
    if verbose:
      print("Added chocolate", choc)
    return True

  def move_to_adjacent_random(self, verbose=False, typeID = -1):
    """
      Randomly moves a chocolate in the box to an adjacent position
      
      returns the initial and final chocolates. Nones if failed.
    """
    choc_initial = None
    choc_final = None
    if self.__nb_chocolates == 0:
      warnings.warn("Box is empty", Warning)
      return choc_initial, choc_final
    if self.__nb_chocolates == self.__size:
      warnings.warn("Box is full", Warning)
      return choc_initial, choc_final
    if typeID < 0:
      possible_ids = [ID for ID in self.__gui.get_possible_chocolate_types() 
                      if self.__nb_chocolates_per_type[ID] != 0]
      if len(possible_ids) == 0:
        warnings.warn("No chocolates left for selected types.", Warning)
        return choc_initial, choc_final
      typeID = random.choice(possible_ids)
    if len(self.__on_chocolates_per_type[typeID]) == 0:
      warnings.warn("No chocolates left for selected type.", Warning)
      return choc_initial, choc_final
    choc_initial = random.choice(self.__on_chocolates_per_type[typeID])
    choc_final = self.move_to_adjacent(choc_initial, verbose)
    if choc_final is None:
      choc_initial = None
    return choc_initial, choc_final

  def move_to_adjacent(self, choc_initial, verbose=False, raise_if_false=True):
    """ Moves a specified chocolate to a randomly chosen adjacent position
    
      returns the final chocolate. If failed, final choc is None
    """
    choc_final = None
    row_initial = choc_initial.get_row()
    column_initial = choc_initial.get_column()
    possible_rowcols = [ [row_initial+1,column_initial],
                         [row_initial,column_initial+1],
                         [row_initial-1,column_initial],
                         [row_initial,column_initial-1] ]
    possible_rowcols = [ [r,c] for r,c in possible_rowcols
                         if self._check_row_column(
                           r,
                           c,
                           verbose=False,
                           raise_if_false=False
                         )
                         and not self[r,c].is_on() ]
    if len(possible_rowcols) > 0:
      row_final,column_final = random.choice(possible_rowcols)
      choc_final = self[row_final,column_final]
      if not self.move_chocolate(choc_initial,
                                 choc_final,
                                 verbose=verbose,
                                 raise_if_false=raise_if_false):
        choc_final = None
    return choc_final

  def move_random_chocolate(self, verbose=False, raise_if_false=True, typeID = -1):
    """
      Randomly moves a chocolate in the box.
      
      Returns the initial and final choclate. Return Nones if failed.
    """
    choc_initial = None
    choc_final = None
    if self.__nb_chocolates == 0:
      warnings.warn("Box is empty", Warning)
      return choc_initial,choc_final
    if self.__nb_chocolates == self.__size:
      warnings.warn("Box is full", Warning)
      return choc_initial,choc_final
    if typeID < 0:
      possible_ids = [ID for ID in self.__gui.get_possible_chocolate_types() 
                      if self.__nb_chocolates_per_type[ID] != 0]
      if len(possible_ids) == 0:
        warnings.warn("No chocolates left for selected types.", Warning)
        return choc_initial, choc_final
      typeID = random.choice(possible_ids)
    if len(self.__on_chocolates_per_type[typeID]) == 0:
      warnings.warn("No chocolates left for selected type.", Warning)
      return choc_initial, choc_final
    choc_initial = random.choice(self.__on_chocolates_per_type[typeID])
    choc_final = random.choice(self.__off_chocolates)
    if not self.move_chocolate(choc_initial,
                               choc_final,
                               verbose=verbose,
                               raise_if_false=raise_if_false):
      choc_initial = None
      choc_final = None
    return choc_initial, choc_final

  # (Re)moving methods
  def move_chocolate(
        self,
        choc_initial, choc_final,
        verbose=False,
        raise_if_false=False,
      ):
    """ Changes an ON chocolate to OFF and vice versa 
    
    Returns True on sucess, False on failure
    """
    if choc_initial == choc_final:
      return True
    if not choc_initial.is_on():
      warnings.warn("There is no chocolate here, can not move it.", Warning)
      return False
    if choc_final.is_on():
      warnings.warn("There is already a chocolate here, can not move there.", Warning)
      return False
    typeID = choc_initial.get_typeID()
    if not self.eat_chocolate(choc_initial,
                              verbose,
                              raise_if_false):
      return False
    if not self.add_chocolate(choc_final,
                              verbose,
                              raise_if_false,
                              typeID=typeID):
      return False
    return True

  def eat_all_selected_types(self, verbose=False, raise_if_false=True):
    possible_ids = [ID for ID in self.__gui.get_possible_chocolate_types() 
                    if self.__nb_chocolates_per_type[ID] != 0]
    if len(possible_ids) == 0:
      warnings.warn("No chocolates left for selected types", Warning)
      return
    while len(possible_ids) > 0:
      self.eat_random_chocolate(verbose=False, raise_if_false=raise_if_false)
      possible_ids = [ID for ID in self.__gui.get_possible_chocolate_types() 
                      if self.__nb_chocolates_per_type[ID] != 0]
    if verbose:
      print("Ate all chocolates of selected types.")

  def eat_random_chocolate(self, verbose=False, raise_if_false=True, typeID=-1):
    """
      change a random ON chocolate to OFF chocolate if possible, and 
      returns it. If not possible, returns None
    """
    choc= None
    if self.__nb_chocolates == 0:
      warnings.warn("Box is empty", Warning)
      return choc
    if typeID < 0:
      possible_ids = [ID for ID in self.__gui.get_possible_chocolate_types() 
                      if self.__nb_chocolates_per_type[ID] != 0]
      if len(possible_ids) == 0:
        warnings.warn("No chocolates left for selected types.", Warning)
        return choc
      typeID = random.choice(possible_ids)
    if len(self.__on_chocolates_per_type[typeID]) == 0:
      warnings.warn("No chocolates left for selected type.", Warning)
      return choc
    choc = random.choice(self.__on_chocolates_per_type[typeID])
    if not self.eat_chocolate(choc,
                              verbose=verbose,
                              raise_if_false=raise_if_false):
      choc = None
    return choc

  def eat_chocolate(self, choc, verbose=False, raise_if_false=True):
    """
      Change chocolate at specified row and column to ON.
    
      Updates GUI
      returns True on sucess, False on failure
    """
    if choc is None:
      return False
    if not choc.is_on():
      warnings.warn("No chocolate in specified position", Warning)
      return False
    else:
      self.__nb_chocolates -= 1
      self.__nb_chocolates_per_type[choc.get_typeID()] -= 1
      self.__on_chocolates.remove(choc)
      self.__on_chocolates_per_type[choc.get_typeID()].remove(choc)
      self.__off_chocolates.append(choc)
      self.__gui.erase_chocolate(choc)
      choc.off()
      if verbose:
        print("Ate chocolate", choc)
    return True

  # Inner methods
  def _check_row_column(self, row, column, verbose=True, raise_if_false=True):
    """
      Used in any method trying to access box content to check if the specified
      row and column make sense. Can choose between return if false or
      raising a ValueError if not.
    """
    if row >= self.__nb_row or row < 0:
      if raise_if_false:
        raise ValueError("Coordinate out of bounds")
      else:
        if verbose:
          warnings.warn("Coordinate out of bounds", Warning)
        return False
    if column >= self.__nb_column or column < 0:
      if raise_if_false:
        raise ValueError("Coordinate out of bounds")
      else:
        if verbose:
          warnings.warn("Coordinates out of bounds", Warning)
        return False
    return True
