def a_over_r2(r, A=1):
  """ Classical electric-like central potential
  
  r is the distance to the origin
  A is a constant
  """
  
  return -A/r**2

def spring(x, k=1):
  """ Classical spring potential
  
  x is the distance to the origin
  k is the spring constant
  """
  
  return kx**2/2

def lennard_jones(r, e=1, s=1):
  """ Lennard Jones potential 
  
  r is the distance to the origin
  e is the potential depth
  s is the finite distance at which the inter-chocolate potential is zero
  
  see https://en.wikipedia.org/wiki/Lennard-Jones_potential
  """
  
  return 4*e*((s/r)**12 - (s/r)**6)

def earth_gravity(z, g=1):
  """ Classical earth gravity-like potential
  
  z is the distance to the origin
  g is the gravitationnal constant
  """
  
  return -g*z
