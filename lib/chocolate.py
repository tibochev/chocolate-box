import math

import potentials as pot

class chocolate(object):

  def __init__(self, **kwargs):
    self.__onoff = kwargs.get("onoff")
    self.__row = kwargs.get("row")
    self.__column = kwargs.get("column")
    self.__typeID = kwargs.get("typeID",None)
    if self.__onoff == "on":
      self.__onoff = "ON"
    if self.__onoff == "off":
      self.__onoff = "OFF"
    if self.__onoff != "ON" and self.__onoff != "OFF":
      raise ValueError("onoff must be \"ON\" or \"OFF\"")
    if self.__onoff == "ON" and self.__typeID is None:
      raise ValueError("Must provide a chocolate type ID")

  def __str__(self):
    return "".join((self.__onoff,":(",
                    str(self.__row),",",str(self.__column),") ",
                    "typeID=",str(self.__typeID)
                    ))

  def __eq__(self, choc):
    if self.__row == choc.get_row() and self.__column == choc.get_column():
      return True
    else:
      return False

  def __ne__(self, choc):
    if self.__row != choc.get_row() or self.__column != choc.get_column():
      return True
    else:
      return False

  def get_coordinates(self):
    return [self.__row, self.__column]

  def get_row(self):
    return self.__row

  def get_column(self):
    return self.__column

  def get_typeID(self):
    return self.__typeID

  def is_on(self):
    if self.__onoff == "ON":
      return True
    else:
      return False

  def set_coordinates(self, coordinates):
    self.__row = coordinates[0]
    self.__column = coordinates[1]

  def set_row(self, row):
    self.__row = row

  def set_row(self, column):
    self.__column = column

  def set_typeID(self, typeID):
    self.__typeID = typeID

  def on(self):
    self.__onoff = "ON"

  def off(self):
    self.__onoff = "OFF"
    self.__typeID = None
