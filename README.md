# chocolate-box
authors:
  * CHEVALERIAS Thibault 
  * COTTE Philippe cottephi@gmail.com


Stupid chocolate box ordering problem

* create list of list N1 x N2
* aim: find symmetric orderings of n chocolates using a potential energy approach
* can use different potential energies (central, gravitation-like, Van der Vaals...)
* first method - brute force: try all orderings and get the best ones
* second method - Monte-Carlo: start with random ordering and perform random steps if energy decreases or if random number chosen between 0 and 1 is below exp(-delta_energy)

* Try odd and even box sizes
